import formatError from "./requestFilter"
import {GlobalSetting} from "@/GlobalSetting";

const md5 = require('md5');
const globalSetting: GlobalSetting = GlobalSetting.getInstance();
const baseUrl = globalSetting.getEnvironment(); 

const request = (method: any, url: any, data: any) => {
	//设置请求头
	const header = {
		'LOCATION': globalSetting.getUserLatLng().lat + "," + globalSetting.getUserLatLng().lng,
		'mobile-os': "wechat_mini",
		'device-id': globalSetting.getUserOpenId(),
		'api-version': 1.2,
		'device-token': md5(globalSetting.getUserOpenId() + 'jiangzhen-t1g'), 
	}
	//promise封装一层，使得调用的时候直接用then和catch接收
	return new Promise((resolve, reject) => {
		uni.request({
			method: method,
			url: baseUrl + url, //完整的host
			data: data,
			header: header,
			success(res) {
				//对成功返回的请求进行数据管理和统一逻辑操作
				// let status:any = res.data.status
				if ((res.data as any).status === 1) { //请求返回成功
					if (res.data) { //后端对接口请求处理成功，返回数据给接口调用处
						resolve((res.data as any).data) //then接收
					}
				} else {
					formatError(res.data) //统一的报错处理逻辑
					reject(res.data) //catch接收
				}
			},
			fail(err) {
				uni.showToast({
					title: '网络异常，稍后再试！',
					mask: true,
					icon: 'none',
					duration: 3000
				})
			}
		})
	})
}
export default request;