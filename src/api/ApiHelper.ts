import {Api} from "@/api/Api";
import request from "./request";
import {AddrBook, ExistedAddrBook} from "@/module/AddrBook";
import formatError from "./requestFilter";
import {PlaceOrderParams} from "@/module/PlaceOrderParams";

const GOOGLE_API_KEY = "AIzaSyDAHDRf520FLI-CtRx8snkJYA8nXYVQZ8I"

export class ApiHelper{
	private static apiHelper: ApiHelper;
	  
	public static getInstance() {
	    if (!this.apiHelper) {
	      this.apiHelper = new ApiHelper();
	    }
	    return this.apiHelper;
	}
	 
    private getRequest(api: string, params = {}) {
		return request("GET", api, params)
    }
	
	private postRequest(api: string, params = {}) {
		return request("POST", api, params)
	}
	
	// 根据坐标获取用户所在城市
	getCustomerCity(customer_latlng: string){
		let params = {'customer_latlng': customer_latlng}
		return this.getRequest(Api.getCustomerCity , params);
	}
	
	//获取城市列表
	  public getCityList() {
	    return this.getRequest(Api.area);
	}
	
	//获取商户列表
	public getShopListV2(city_id: number, lat: number, lng: number) {
		let params = {
		  'city_id': city_id,
		  'lat': lat,
		  'lon': lng,
		}
		return this.getRequest(Api.shopList, params)
	}
	
	//获取商家列表
	public getShopMenu(mid: number, deliveryType: string){
		let params = {
			'shop_id': mid,
			'shipping_type': deliveryType
		}
		return this.getRequest(Api.shopNewMenu, params)
	}
	
	//获取商家详情
	public getshopDetail(mid: number){
		let params = {'mid': mid}
		return this.getRequest(Api.shopInfo, params)
	}
	
	//发送手机验证码
	public sendVerifyCode(phone_num: string) {
		const params = {
		  "phone_num": phone_num
		}
		return this.getRequest(Api.sendCode, params);
	}
	
	//验证手机验证码
	public checkVerifyCode(phone_num: string, code: string) {
		let params = {
		  "phone_num": phone_num,
		  "code": code,
		}
		return this.postRequest(Api.verifyCode, params);
	}
	
	//登录接口
	public login(username: string, password: string) {
		let params = {
		  "username": username,
		  "password": password,
		}
		return this.postRequest(Api.login, params);
	}
	
	public getAddressBook() {
	    return this.getRequest(Api.addressList);
	}
	
	public postNewAddressBook(addrBook: AddrBook) {
		return this.postRequest(Api.addAddress, addrBook);
	}

	public postUpdateAddressBook(addrBook: ExistedAddrBook) {
		return this.postRequest(Api.updateAddress, addrBook);
	}

	public postDeleteAddressBook(id: number) {
		const params = {
		  id: id
		}
		return this.postRequest(Api.deleteAddress, params);
	}
	
	public fetchAddressLatLng(addr: string){
		const params = {
		  address: addr
		}
		return this.getRequest(Api.getCustomerCity, params)
	}
	
	public isOutOfRange(addr: string, latLon: string, ds_id: string){
		const params = {
		  address: addr,
		  shop_latlng: latLon,
		  ds_id: ds_id
		}
		return this.getRequest(Api.getCustomerCity, params)
	}
	
	public calCart(shop_id: number, shipping_type: string, address: string, subtotal: number, 
		promo_codes: string, auto_match: number, product_items: string, open_membership: number){
		const params = {
		  shop_id: shop_id,
		  address: address,
		  subtotal: subtotal,
		  promo_codes: promo_codes,
		  auto_match: auto_match,
		  product_items: product_items,
		  open_membership: open_membership,
		}
		return this.postRequest(Api.calculate, params)
	}
	
	public fetchOrderList(){
		return this.getRequest(Api.order_history)
	}
	
	public getOrderDetail(orderId: number) {
	    const params = {
	      "order_id": orderId
	    }
	    return this.getRequest(Api.order_detail, params);
	}
	
	public sendReceiptRequest(order_id:number,email:string){
	    const params ={
	      "order_id": order_id,
	      "email": email,
	    }
	    return this.postRequest(Api.sendReceipt, params)
	}
	
	public placeOrder(o: PlaceOrderParams){
		return this.postRequest(Api.placeOrder, o)
	}
	
	public snapPay(orderId: number, pay_user_account_id: string, appid: string){
		const params = {
		  order_id: orderId,
		  pay_user_account_id: pay_user_account_id,
		  appid: appid
		}
		return this.getRequest(Api.snappay, params);
	}
	
	public fetchCreditCardList(){
		return this.getRequest(Api.creditCardList);
	}
	
	public deleteCard(id: number){
		const params = {id: id}
		return this.postRequest(Api.deleteCreditCard, params)
	}
	
	public async fetchUserDetail(cid: number){
		const params = {cid: cid}
		return this.getRequest(Api.userDetail, params)
	}
	
	public async addCreditCard(p: object){
		return this.postRequest(Api.addCreditCard, p)
	}
	
	public async getPaymentToken(p:object){
		return this.postRequest(Api.getPaymentToken, p)
	}
	
	public async create_charge(token: string, order_id: number, cvv: string){
		const params = {
			charge_token: token,
			order_id: order_id,
			cvc: cvv
		}
		return this.postRequest(Api.create_charge, params)
	}
	
	public async hurryUp(order_id: number){
		const params = {
			order_id: order_id
		}
		return this.postRequest(Api.hurryUp, params)
	}
	
	public async fetchCouponList(p: object){
		return this.getRequest(Api.couponList, p)
	}
	
	public async verify_coupon(p: object){
		return this.postRequest(Api.verify_coupon, p)
	}
	
	public async checkPromo(p: object){
		return this.postRequest(Api.promocode, p)
	}
	
	public async getPromocodeList(p: object){
		return this.getRequest(Api.promocodeList, p)
	}
	
	public getSearchAddress(text: string){
		return new Promise((resolve, reject) => {
			uni.request({
				method: 'GET',
				url:  Api.googleMap, //完整的host
				data: {
					key: GOOGLE_API_KEY,
					input: text,
					language: "en-CA",
					components: "country:ca"
				},
				success(res) {
					// @ts-ignore
					if(res.data.status === "OK"){
						resolve(res.data)
					}else{
						formatError(res.data) //统一的报错处理逻辑
						reject(res.data) //catch接收
					}
				},
				fail(err) {
					uni.showToast({
						title: '网络异常，稍后再试！',
						mask: true,
						icon: 'none',
						duration: 3000
					})
				}
			})
		})
	}
	   
}