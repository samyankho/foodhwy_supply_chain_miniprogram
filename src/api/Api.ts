export enum Api{
	area = '/area',
	shopList = '/shoplist/v2',
	shopInfo = '/shop',
	shopMenu = '/menu_categories',
	shopNewMenu = '/shop_menu',
	
	getCustomerCity = "/area/customer_latlng",	//根据用户经纬度获得所在城市，以及判断是否在送餐范围内
	sendCode = '/request_verificode',
	verifyCode = '/check_verificode',
	login = '/login',
	
	addAddress = '/user/addrbook/new',
	deleteAddress = '/user/addrbook/delete',
	updateAddress = '/user/addrbook/update', 
	addressList = '/user/addrbook/list',
	googleMap = 'https://maps.googleapis.com/maps/api/place/autocomplete/json',
	
	placeOrder = '/place_order',
	cal_shipping_fee = '/cal_shipping_fee',
	order_history = '/order_history',
	order_detail = '/order_detail',
	
	getPaymentToken = '/payment/credit_card/get_token',
	create_charge = '/payment/create_charge',
	
	promocode = '/promo/validate',
	promocodeList = '/user/coupons/list/v2',
	shopPromo = '/promo/check_orderscount',
	
	userDetail = '/user/detail',
	hurryUp = '/hurry_up',
	
	creditCardList = '/user/credit_card/list',
	addCreditCard = '/user/credit_card/add',
	deleteCreditCard = '/user/credit_card/delete',
	sendReceipt = '/user/receipt/request',
	
	rate = '/review_order/v2',
	calculate = '/cart/calculate',
	snappay = '/payment/snappay/minipay',
	couponList = '/user/coupons/list/group',
	verify_coupon = '/promo/validate/coupons',
	search = '/search',
}