import {ApiHelper} from "@/api/ApiHelper";
import {CreditCard} from "@/module/CreditCard";

export class CreditCardLogic{
	private static creditCardLogic: CreditCardLogic;
	private apiHelper = ApiHelper.getInstance();
	
	private CreditCardList: CreditCard[] = []
	private payment_gateway: string = ""
	private order_id: number = 0
	
	public static getInstance() {
	  if (!this.creditCardLogic) {
	    this.creditCardLogic = new CreditCardLogic();
	  }
	  return this.creditCardLogic;
	}
	
	public async fetchCreditCardList(){
		const response: any = await this.apiHelper.fetchCreditCardList()
		this.CreditCardList = response
	}
	
	public getCreditCardList(){
		return this.CreditCardList;
	}
	
	public async deleteCard(id: number){
		const response: any = await this.apiHelper.deleteCard(id)
		return response;
	}
	
	public async addCreditCard(p: object){
		const response: any = await this.apiHelper.addCreditCard(p)
		return response;
	}
	
	public async fetchOrderDetail(order_id: number){
		const response: any = await this.apiHelper.getOrderDetail(order_id)
		this.payment_gateway = response.payment_gateway
		response.order_id = this.order_id
		return response;
	}
	
	public async getPaymentToken(p: any){
		p.payment_gateway = this.payment_gateway
		console.log(p)
		const response: any = await this.apiHelper.getPaymentToken(p)
		return response
	}
	
	public async create_charge(token: string, order_id: number, cvc: string){
		const response: any = await this.apiHelper.create_charge(token, order_id, cvc)
		return response;
	}
	
}