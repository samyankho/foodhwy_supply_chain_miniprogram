import {PaymentStatus} from "@/module/PaymentStatus";
import {OrderStatus} from "@/module/OrderStatus";
import {ApiHelper} from "@/api/ApiHelper";
import {getNewOrderDetail, OrderDetail} from "@/module/OrderDetail";


export class OrderDetailLogic {

  private static orderDetailLogic: OrderDetailLogic;
  private apiHelper = ApiHelper.getInstance();
  private OrderDetail: OrderDetail = getNewOrderDetail();

  public static getInstance() {
    if (!this.orderDetailLogic) {
      this.orderDetailLogic = new OrderDetailLogic();
    }
    return this.orderDetailLogic;
  }
  
  getOrderStatus(status: number){
  	switch (status) {
  	    case -30:
  	        return "拼单";
  	    case -20:
  	        return "预订单";
  	    case -10:
  	        return "待兑换";
  	    case -1:
  	        return "已取消";
  	    case 0:
  	        return "进行中";
  	    case 1:
  	        return "等待付款";
  	    case 10:
  	        return "已确认";
  		case 11:
  		    return "出餐中";
  		case 12:
  		    return "已取餐";
  		case 15:
  		    return "出餐中";
  		case 20:
  		    return "已完成";
  	} 
  }
  
 getPaymentStatus(status: number){
 	if(status === -1 || status === 0){
 		return '未支付'
 	}else if(status === 10){
 		return '预支付'
 	}else if(status === 20){
 		return '已支付'
 	}else if(status === 30){
 		return '退款'
 	}else if(status === 99){
 		return '支付失败'
 	}
 }

  public async fetchOrderDetail(orderId: number) {
    try {
      const response: any = await this.apiHelper.getOrderDetail(orderId);
      this.OrderDetail = response;
    } catch (e) {
      console.log(e);
    }
  }

  public getOrderDetail(): OrderDetail {
    return this.OrderDetail;
  }


  public async sendReceipt(order_id:number, email:string){
    try {
      const response: any = await this.apiHelper.sendReceiptRequest(order_id, email);
      return response
    }catch (e) {
      console.log(e);
    }
  }

}
