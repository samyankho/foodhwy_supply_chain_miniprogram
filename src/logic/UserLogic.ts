import {ApiHelper} from "@/api/ApiHelper";
import {UserDetail} from "@/module/UserDetail";

export class UserLogic {
	private static userLogic: UserLogic;
	private apiHelper = ApiHelper.getInstance();
	private userDetail: UserDetail = new UserDetail();

	public static getInstance() {
		if (!this.userLogic) {
		  this.userLogic = new UserLogic();
		}
		return this.userLogic;
	}
	
	public async fetchUserDetail(cid: number){
		const response: any = await this.apiHelper.fetchUserDetail(cid)
		this.userDetail = response
	}
	
	public getUserDetail(){
		return this.userDetail;
	}
	
	public async checkMid(mid: number){
		const response: any = await this.apiHelper.getshopDetail(mid)
		return response
	}
	
}