import {Product} from "@/module/Product";
import {Option} from "@/module/Option";
import {Item} from "@/module/Item";
import {ShopDetail} from "@/module/ShopDetail";
import {GlobalSetting} from "@/GlobalSetting";
import {CouponValidation, CouponValidationProduct, ValidationResult} from "@/module/Coupon";
import {FullDiscountLogic} from "@/logic/FullDiscountLogic";

export class CartLogic{
	private globalSetting: GlobalSetting = GlobalSetting.getInstance();
	protected static cart: CartLogic;
	protected shoppingCart: Product[] = [];
	protected subtotal: number = 0;
	protected orgSubtotal: number = 0;
	private shopId: number = 0;
	private shippingType: string = "";
	private couponCode: string = '';
	private gift = "";
	private shopDetail!: ShopDetail;
	private availableNextHit = 0;
	private nextSaleAmount = 0;
	private nextFullAmount = 0;
	private nextGift = "";
	private discount = 0;
	private perDiscount = 0;
	private productsDiscount = 0;
	private shopProductSaleItems: Product[] = [];
	private shopProductSaleLimit: number = 0;
	private overLimitProductId: number[] = []; //用于外部判断是否已经限制购买

	
	public static getInstance() {
	    if (!this.cart) {
	      this.cart = new CartLogic();
	    }
	    return this.cart;
	}
	
	public isCartEmpty(): boolean{
	    return this.shoppingCart.length === 0
	}
	
	public renewProductInCart(product: Product) {
		for(let i = 0; i < this.shoppingCart.length; i++){
			if(this.isMatchProduct(this.shoppingCart[i], product)){
				this.shoppingCart[i] = product
			}
		}
		this.calculateSubtotal();
		this.calculateOrgSubtotal();
	}
	
	public addToCart(product: Product){
		// console.log(product)
		product.hasSpecialOffer() && product.specialOfferEnable() ? this.addProductSaleDiscount(product) : this.add(product);
		this.addPerDiscount(product);
		this.calRealSubTotal();
		this.calculateSubtotal();
		this.calculateOrgSubtotal();
		// console.log(this.shoppingCart)
	}
	
	//查看是否有商品折扣
	private addProductSaleDiscount(product: Product) {
		// console.log('限购', product)
		this.copyMultipleTimesProductsByQty(product)
		this.productsDiscount = this.calProductDiscountPrice();
	}
	
	private calProductDiscountPrice() {
	    return this.shopProductSaleItems.reduce((p, c) => {
	      c.dis_qty = 1;
	      return p + c.getDiscountPrice()
	    }, 0)
	}
	
	private copyMultipleTimesProductsByQty(product: Product) {
	    let times = product.qty;
	    for (var i = 0; i < times; i++) {
	      const copy = product.deepCopyProduct()
	      let del;
	      copy.qty = 1;
	      copy.dis_qty = 0;
	      //先判断商品限购数量，再判断商品组限购数量
	      if (this.isOverSpecialLimit(product)) {
	        this.addToLimitList(product)
	      } else {
	        this.removeFromLimitList(product);
	        //先添加进购物车和打折商品列表
	        this.shopProductSaleItems.push(copy)
	        //如果超过了限制，找到打折力度最大的，从打折列表和购物车中移除，恢复原价
	        if (this.isOverSpecialGroupLimit()) {
	          this.sortProductsSale();
	          del = this.removeFromProductSale()
	        }
	      }
	      let disQty = this.getDiscountProductQty(product);
	      this.setProductDisQty(copy, disQty, del);
	    }
	}
	
	private getDiscountProductQty(product: Product): number {
	    return this.shopProductSaleItems.reduce((p, c) => {
	      if (this.isMatchFromCloudCart(c, product)) {
	        p = p + c.qty
	      }
	      return p;
	    }, 0)
	}
	
	private isMatchFromCloudCart(p1: Product, p2: Product) {
	    return p1.product_id === p2.product_id;
	}
	
	private isOverSpecialLimit(product: Product): boolean {
	    if(product.discount_limit_times ===0 ){
	      return false
	    }
	    if (!this.isInCart(product)) {
	      return false;
	    }
	    let cartProduct: Product[];
	    let limit = product.discount_limit_times
	    let qty = 0;
	    cartProduct = this.searchCart((p) => {
	      if (!this.isMatchProduct(product, p)) {
	        return;
	      }
	      return p;
	    })
	    cartProduct.map(c => {
	      if (!c) {
	        return
	      }
	      qty = qty + c.qty
	    })
	    return limit <= qty
	}
	
	private addToLimitList(product: Product) {
	    if (this.isInLimitList(product)) {
	      return
	    }
	    this.overLimitProductId.push(product.product_id);
	}
	
	public isInLimitList(product: Product): boolean {
		return this.overLimitProductId.includes(product.product_id);
	}

	private removeFromLimitList(product: Product) {
		if (!this.isInLimitList(product)) {
		  return
		}
		let index = this.overLimitProductId.indexOf(product.product_id);
		this.overLimitProductId.slice(index, index + 1);
	}
	
	public addPerDiscount(p: Product){
	    if(!p.isPercentageSale()){
	      return
	    }
	    this.perDiscount = this.globalSetting.roundTowDecimalNumber(this.perDiscount + p.getDiscountPrice())
	}
	
	public reducePerDiscount(p:Product){
	    if(!p.isPercentageSale()){
	      return
	    }
	    this.perDiscount = this.perDiscount - p.getDiscountPrice();
	    this.perDiscount = this.perDiscount <=0 ? 0 : this.globalSetting.roundTowDecimalNumber(this.perDiscount);
	}
	
	private calRealSubTotal() {
	    const disObj = FullDiscountLogic.getInstance().checkShopFullDiscount(this.globalSetting.roundTowDecimalNumber(
	      // @ts-ignore
	    this.orgSubtotal - this.productsDiscount - this.perDiscount), this.shopDetail)
		// console.log(disObj)
	    this.discount = disObj.discount
	    this.setFullDiscountParams(disObj)
	    this.subtotal = this.globalSetting.roundTowDecimalNumber(this.orgSubtotal - this.discount - this.productsDiscount - this.perDiscount);
	    this.subtotal = this.subtotal > 0 ? this.subtotal : 0;
	    this.availableNextHit = this.globalSetting.roundTowDecimalNumber(this.nextFullAmount - this.subtotal);
	    this.setCartStorge();
	}
	
	private setCartStorge(){
		uni.setStorage({
		    key: 'shopping_cart',
		    data: JSON.stringify(this.shoppingCart),
		});
	}
	
	private setFullDiscountParams(obj: object){
	    // @ts-ignore
	    this.nextSaleAmount = obj.nextSaleAmount
	    // @ts-ignore
	    this.nextFullAmount = obj.nextFullAmount
	    // @ts-ignore
	    this.nextGift = obj.nextGift
	    // @ts-ignore
	    this.gift = obj.gift
	}
	
	/**
	   * 负数代表没有超过限制，正数表示超过了限制
	   */
	private isOverSpecialGroupLimit() {
		if (this.shopProductSaleLimit <= 0) {
		  return false
		}
		let qty = this.shopProductSaleItems.length
		return qty - this.shopProductSaleLimit > 0;
	}

	private sortProductsSale(): Product[] {
		//从小到大按价格排列
		return this.shopProductSaleItems.sort((a, b) => {
		  return a.getDiscountAmountPerProduct() - b.getDiscountAmountPerProduct()
		})
	}

	private removeFromProductSale(): Product {
		//单价打折最多的商品
		let length = this.shopProductSaleItems.length
		let item = this.shopProductSaleItems[length - 1];
		this.shopProductSaleItems = this.shopProductSaleItems.slice(0, length - 1)
		return item;
	}
	
	  // private setProductToOrgPrice(product: Product) {
	  //   //先减去一个， 再加上一个原价
	  //   // super.reduce(product);
	  //   product.deepCopyProduct();
	  //   product.is_special = 0;
	  //   product.qty = 1;
	  //   product.price = product.org_price;
	  //   super.add(product)
	  // }
	
	private setProductDisQty(product: Product, disQty: number, del?: Product) {
		console.log(product, disQty, del)
		this.add(product)
		this.setDiscountProductToOrgPrice(product, del);
		this.setDiscountProductToDiscountPrice(product, disQty)
	}
	
	private setDiscountProductToOrgPrice(product: Product, del?: Product) {
	    //带有选项的菜被挤出
	    if (!del || del.product_id === product.product_id) {
	      return
	    }
	    if (del.hasOption()) {
	      let ps = this.shoppingCart.filter(p => {
	        return this.isMatchFromCloudCart(p, del)
	      })
	      ps[0].dis_qty = 0
	      return;
	    }
	    let ps = this.shoppingCart.filter(p => {
	      return this.isMatchFromCloudCart(p, del)
	    })
	    ps[0].dis_qty = ps[0].dis_qty - 1
	}
	
	private setDiscountProductToDiscountPrice(product: Product, disQty: number) {
	    if (product.hasOption()) {
	      //带有选项的
	      let ps = this.shoppingCart.filter((p: any) => {
	        return this.isMatchFromCloudCart(p, product)
	      })
	      ps.length > 0 ? ps.map((p:any) => {
	        if (ps.indexOf(p) < disQty) {
	          p.dis_qty = 1;
	        }
	      }) : null
	      return
	    }
	    //不带有选项的
	    this.searchCart(p => {
	      if (this.isMatchFromCloudCart(p, product)) {
	        p.dis_qty = disQty;
	      }
	    })
	}
	
	public add(product: Product){
		// console.log('普通添加', product)
		//添加购物车带option逻辑
		if (this.hasOptions(product)) {
		  this.addProductWithOption(product)
		} else {//不带选项逻辑
		  this.addProductWithOutOption(product);
		}
	}
	
	private addProductWithOption(product: Product) {
	    var qty = product.qty
	    //待选项的商品在购物车中独立存在 qty != 1
	    for (var i = 0; i < qty; i++) {
	      const copy = product.deepCopyProduct()
	      copy.qty = 1;
	      this.shoppingCart.push(copy)
	    }
	}

	private addProductWithOutOption(product: Product) {
		const productCopy = product.deepCopyProduct();
		this.isInCart(productCopy)
		  ? this.addProductQuantity(productCopy)
		  : this.shoppingCart.push(productCopy)
	}
	
	protected isInCart(product: Product) {
		var inInCart = false;
		this.searchCart(p => {
		  if (this.isMatchProduct(p, product)) {
			inInCart = true;
		  }
		})
		return inInCart;
	}
	
	private addProductQuantity(product: Product) {
	    this.searchCart(p => {
	      if (this.isMatchProduct(p, product)) {
	        // @ts-ignore
	        p.qty = p.qty + product.qty
			p.row_total = p.calculateRowTotal()
			p.org_row_total = p.calculateOrgRowTotal()
	        this.updateProductNote(p, product)
	      }
	    })
	}
	
	public removeItem(product: Product) {
	    if (!this.isInCart(product)) {
	      return;
	    }
		this.remove(product)
		product.hasSpecialOffer() && product.specialOfferEnable()? this.rmProductSaleDiscount(product) : null
		this.reducePerDiscount(product)
		this.checkCartEmpty();
	}
	
	private checkCartEmpty() {
	    if (!this.isCartEmpty()) {
	      this.calRealSubTotal();
	      return;
	    }this
	    this.clearCart();
	}
	
	private rmProductSaleDiscount(product: Product) {
	    this.removeFromProductSaleList(product)
	    this.addProductBack();
	    this.productsDiscount = this.calProductDiscountPrice()
	}
	
	private removeFromProductSaleList(p: Product) {
	    this.shopProductSaleItems = this.shopProductSaleItems.filter(s => {
	      return !this.isMatchProduct(s, p)
	    })
	}
	
	private addProductBack() {
	    // 筛选出打折菜品
	    let sp = this.getCart().filter(p => {
	      //打折商品，而且是需要限购的打折商品
	      return p.hasSpecialOffer() && p.specialOfferEnable();
	    })
	    //不打折的菜先加入购物车
	    this.setCart(this.getCart().filter(p => {
	      //没有开启商品限购的打折商品
	      return !p.specialOfferEnable() ;
	    }))
	
	    //重新添加打折菜品并计算
	    sp.map(p => {
	      this.rmProductSaleDiscount(p)
	      this.addToCart(p)
	    })
	
	}
	
	public remove(product: Product) {
	    if (this.hasOptions(product)) {
	      this.removeProductWithOption(product);
	    } else {
	      this.removerProductWithoutOption(product);
	    }
		this.calculateSubtotal();
	    this.calculateOrgSubtotal();
	}
	
	protected removeProductWithOption(product: Product) {
	    var index = this.shoppingCart.findIndex(p => {return this.matchProductWithOption(p, product)})
	    this.shoppingCart.splice(index, 1);
	}
	
	private matchProductWithOption(p1: Product, p2: Product) {
	    return this.isMatchOptions(p1.options, p2.options);
	}
	
	private isMatchOptions(o1: any, o2: any) {
	    return JSON.stringify(o1) === JSON.stringify(o2);
	}
	
	protected removerProductWithoutOption(product: Product) {
		this.searchCart(p => {
		  if (this.isMatchProduct(p, product)) {
		    p.qty--;
			if(p.qty === 0){
				this.shoppingCart = this.shoppingCart.filter(p =>{return p.qty !== 0})
			}else{ 
				p.row_total = p.calculateRowTotal()
				p.org_row_total = p.calculateOrgRowTotal()
			}
		  }
		})
	}
	
	public checkProductQty(product_id: number){
		let count = 0;
		this.shoppingCart.map(p => {
			p.product_id === product_id ? count += p.qty : ''
		})
		return count;
	}
	
	private keepProducts(p1: Product, p2: Product) {
	    return !this.isMatchProduct(p1, p2);
	}
	
	protected isMatchProduct(product1: Product, product2: Product) {
	    return product1.product_id === product2.product_id;
	}
	
	private hasOptions(product: Product) {
	    return product.hasOption();
	}
	
	public setUpCart(obj: any) {
	    this.clearCart();
	    this.setDeliveryType(obj.shippingType);
	    this.setShopId(obj.shopId)
	    this.setShopDetail(obj.shopDetail)
	}
	
	public getCouponValidationProducts(): CouponValidationProduct[] {
	    const products = this.getCart();
	    return products.map((product) => {
	      const couponValidationProduct: CouponValidationProduct = {
	        product_id: product.product_id,
	        options: product.options,
	        note: product.note, 
	        qty: product.qty,
	        row_total: product.row_total
	      }
	      return couponValidationProduct
	    });
	}
	
	public setShopDetail(info: ShopDetail) {
	    this.shopDetail = info;
	    this.hasShopProductSale() ? this.setProductSaleLimit(
	      // @ts-ignore
	      this.shopDetail.data.discount_events?.shop_product_sale[0].options.order_limit_times
	    ) : null
	}
	
	private setProductSaleLimit(limit: number) {
	    this.shopProductSaleLimit = limit;
	}
	
	//打折商品逻辑
	private hasShopProductSale(): boolean {
		// @ts-ignore
		return this.getShopDetail().data.discount_events && this.getShopDetail().data.discount_events.shop_product_sale
	}
	
	/**
	* 清空购物车
	*/
	public clearCart() {
		this.nextSaleAmount = 0;
		this.nextFullAmount = 0;
		this.availableNextHit = 0;
		this.gift = ""
		this.nextGift = ""
		this.discount = 0;
		this.productsDiscount = 0;
		this.shopProductSaleItems = []
		this.shopProductSaleLimit = 0;
		this.shopProductSaleItems = [];
		this.shopProductSaleLimit = 0;
		this.overLimitProductId = [];
		this.subtotal = 0;
		this.couponCode = "";
		this.perDiscount = 0;
		this.shoppingCart = [];
		this.calculateSubtotal();
		this.calculateOrgSubtotal();
	}
	
	public calculateSubtotal(){
		if(this.isCartEmpty()){
		  this.subtotal = 0;
		  return;
		}
		this.subtotal = this.searchCart(p=>{return p.calculateRowTotal()}).
		reduce(
		  function(prev:number,cur:number){
		  return prev + cur;
		},0);
	}
	
	public calculateOrgSubtotal(){
		if(this.isCartEmpty()){
		  this.orgSubtotal = 0;
		  return;
		}
		this.orgSubtotal = this.searchCart(p=>{return this.calculateSubLogic(p)}).
		reduce(
		  function(prev:number,cur:number){
		  return prev + cur;
		},0);
	}
	
	protected searchCart(callback: (any: any) => any) {
	    return this.shoppingCart.map(callback);
	}
	
	protected calculateSubLogic(product: Product){
	    return product.calculateOrgRowTotal();
	}
	
	public setShopId(id: number) {
	    this.shopId = id;
	}

	public getShopId(): number {
		return this.shopId;
	}
	
	public getDeliveryType(): string {
	    return this.shippingType
	}
	
	public setDeliveryType(type: string) {
		this.shippingType = type;
	}
	
	// public setShopDetail(info: ShopDetail) {
	//     this.shopDetail = info;
	// }
	
	public getShopDetail(): ShopDetail {
	    return this.shopDetail;
	}
	
	private updateProductNote(p1: Product, p2: Product){
	     p1.note = p2.note
	}
	
	public getSubTotal(): number {
	    return this.subtotal;
	}
	
	public getOrgSubtotal(): number {
	    return this.orgSubtotal;
	}
	
	public setCart(ps: Product[]){
	    this.shoppingCart = ps
	}
	
	public getCart() {
	    return this.shoppingCart;
	}
	
	public getProductQTYinCart(pid: number){
		let num = 0
		this.shoppingCart.map(p => {
			if (p.product_id === pid) {
			  num = p.qty;
			}
		})
		return num;
	}
	
	//检查是否支持自取
	public checkEnablePickup() {
		return this.getShopDetail().customer_pickup === 1
	}
	
	public setCouponCode(code: string) {
	    this.couponCode = code;
	}

	public getCouponCode(): string {
		return this.couponCode;
	}
	
	public getShopFullAmountGifts(): string {
		return this.gift
	}

	public setShopFullAmountGifts(g: string) {
		this.gift = g;
	}
	
	public inCartDisNumber(id: number): number {
	    const counter = this.shopProductSaleItems.reduce((p, c) => {
	      if (c.product_id === id) {
	        p = p + c.dis_qty
	      }
	      return p;
	    }, 0)
	    return counter
	}
	
}