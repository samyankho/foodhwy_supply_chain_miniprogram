import {ApiHelper} from "@/api/ApiHelper";
import {Shop} from "@/module/Shop";
import {CartLogic} from "@/logic/CartLogic";
import {Product, ProductParam} from "@/module/Product";
import {Menu, SingleMenuParams} from "@/module/Menu";
import {MenuResponse} from "@/module/MenuResponse";
import {DeliveryType} from "@/module/DeliveryType";
import {ShopDetail} from "@/module/ShopDetail";

export class MenuLogic{
	
	private apiHelper = ApiHelper.getInstance();
	private static muneLogic: MenuLogic;
	private cart = CartLogic.getInstance();
	
	private deliveryType: string = DeliveryType.DELIVERY;
	private shopId: number = 0;
	public menuContents: Array<Menu> = [];
	// private cart: SyncApiCart = SyncApiCart.getInstance();
	private shopDetail!: ShopDetail;
	private indexId:number[] = [];
	private selectedProductCopy: Product = new Product();
	private selectedProduct: Product = new Product();
	
	public static getInstance(){
		if(!this.muneLogic){
			this.muneLogic = new MenuLogic();
		}
		return this.muneLogic;
	}
	
	public addToCart(product: Product): void {
	    if (this.isCartEmpty()) {
	      this.initCart();
	    }
	    //如果换店先清空购物车
	    if(this.shopId !== this.cart.getShopId()){
	      this.cart.setShopId(this.shopId)
	      // this.cart.clearCart();
	    }
		product.row_total = product.calculateRowTotal()
		product.org_row_total = product.calculateOrgRowTotal()
	    this.cart.addToCart(product)
	}
	
	public removeFromCart(product: Product): void {
	    this.cart.removeItem(product)
	}
	
	public renewProductInCart(product: Product): void {
	    this.cart.renewProductInCart(product)
	}
	
	public setSelectedProduct(category_index: number, product_index: number){
		this.selectedProduct = this.menuContents[category_index].products[product_index]
		this.selectedProduct.uncheckAllOption()
		this.selectedProductCopy = this.selectedProduct.deepCopyProduct()
		this.selectedProductCopy.qty = 1
	}
	
	public getSelectedProduct(){
		return this.selectedProductCopy
	}
	
	public initCart() {
	    this.cart = CartLogic.getInstance();
	    let obj = {
	      "shopId": this.shopId,
	      "shippingType": this.deliveryType,
	      "shopDetail": this.shopDetail,
	    }
	    this.cart.setUpCart(obj);
	}
	
	public clearCart(){
		this.cart.clearCart();
	}
	
	public isCartEmpty(): boolean{
	    return this.cart.isCartEmpty();
	}
	
	public initProducts(list: ProductParam[]): Product[] {
		let pList: Product[] = []
		list.map(p => {
		  let pdt = Product.initProduct(p)
		  pList.push(pdt)
		})
		return pList
	}
	
	public async fetchMenu(mid: number){
		this.clearCart()
		const result: any = await this.apiHelper.getShopMenu(mid, this.deliveryType)
		this.menuContents = await result.menus.map((r: SingleMenuParams) => {
		        r.products = this.initProducts(r.products);
		        return new Menu(r);
		      });
	}
	
	public getMenu(){
		return this.menuContents
	}
	
	public async fetchShopDetail(mid: number){
		this.shopId = mid
		const res: any = await this.apiHelper.getshopDetail(mid)
		this.shopDetail = res
		this.cart.setShopDetail(res)
	}
	
	public getshopDetail(){
		return this.shopDetail
	}
	
	public setDeliveryType(type: string){
		this.deliveryType = type
	}
	
	public getProductQTYinCart(pid: number){
		return this.cart.getProductQTYinCart(pid)
	}
	
	public productExceedMax(p: Product){
		let count = this.cart.checkProductQty(p.product_id)
		return p.max === 0 ? false : count + p.qty > p.max
	}
	
}