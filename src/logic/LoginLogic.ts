import {ApiHelper} from "@/api/ApiHelper";
import {GlobalSetting} from "@/GlobalSetting";
import {User} from "@/module/User";
import {Contact} from "@/module/Contact";

export class LoginLogic{
	
	protected static currentUserProfile: LoginLogic;
	protected apiHelper = ApiHelper.getInstance();
	protected user!: User;
	
	public static getInstance(){
	    if(!this.currentUserProfile){
	      this.currentUserProfile = new LoginLogic();
	    }
	    return this.currentUserProfile;
	}
	
	//发送验证码
	public async sentCode(phone_num:string) {
		try {
		  await this.apiHelper.sendVerifyCode(phone_num);
		} catch (e) {
		  this.alert(e)
		}
	}

	public async directLogin(phone_num: string, password: string){
		const userInfo: any = await this.apiHelper.login(phone_num, password);
		this.user = userInfo;
		this.setGlobalContact();
	
		//登录后赋值给全局User对象
		let u = User.initUser(this.user)
		return u;
	}

	//正常登录，验证验证码并用返回密码直接登录
	public async login(phone_num:string,code:string) {
		const response: any = await this.apiHelper.checkVerifyCode(phone_num,code);
		const password = response.password
		if(password){
			let u = await this.directLogin(phone_num, password);
			GlobalSetting.getInstance().setUser(u)
		}
	}
	
	protected setGlobalContact(){
	    const contact = new Contact();
	    contact.name = this.user.name
	    contact.addr = this.user.address
	    contact.tel = this.user.phone
	    GlobalSetting.getInstance().setContact(contact);
	}
	
	private alert(msg: string){
		uni.showModal({
			showCancel: false,
			title: '提示',
			content: msg,
		})
	}
	
}