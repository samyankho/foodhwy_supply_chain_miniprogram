import {ApiHelper} from "@/api/ApiHelper";
import {Shop} from "@/module/Shop";
import {City} from "@/module/City";

export class ShopListLogic {

	private apiHelper = ApiHelper.getInstance();
	private static shopListLogic: ShopListLogic;
	private shopList:Shop[] = [];
	private data:Shop[] =[];
	private cities!:City[];
	private currentCity: string = "";
	public currentCityId: number = 0;

	public static getInstance() {
		if (!this.shopListLogic) {
		this.shopListLogic = new ShopListLogic();
		}
		return this.shopListLogic;
	}
	
	public async fetchCityList(){
		const response:any = await this.apiHelper.getCityList()
		this.cities = response.data
	}
	
	public getCityList(){
		return this.cities;
	}
	
	// 获取用户所在城市
	public async fetchCustomerCity(){
		const response:any = await this.apiHelper.getCustomerCity(uni.getStorageSync('userLat') + "," + uni.getStorageSync('userLon'))
		this.currentCity = response.data.memo
		this.currentCityId = response.data.city_id
		// console.log(response)
	}
	
	public getCustomerCity(){
		return this.currentCity;
	}
	
	public getCustomerCityId(){
		return this.currentCityId;
	}
	
	public async fetchShopList(currentCityId: number){
		const response:any = await this.apiHelper.getShopListV2(currentCityId, uni.getStorageSync('userLat'), uni.getStorageSync('userLon'))
		this.shopList = response.data.list
	}
	
	public getShopList(){
		return this.shopList
	}
	
	
	
}