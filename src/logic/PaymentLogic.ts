import {ApiHelper} from "@/api/ApiHelper";
import {Product} from "@/module/Product";
import {PaymentInfo} from "@/module/PaymentInfo";
import {PlaceOrderParams} from "@/module/PlaceOrderParams";

export class PaymentLogic {
	private static paymentLogic: PaymentLogic;
	private apiHelper = ApiHelper.getInstance();
	private paymentInfo: PaymentInfo = new PaymentInfo();
	private orderParams: PlaceOrderParams = new PlaceOrderParams();

	private addrLatLng: string = ""

	public static getInstance() {
		if (!this.paymentLogic) {
		  this.paymentLogic = new PaymentLogic();
		}
		return this.paymentLogic;
	}

	public calTips(percent: number, subtotal: number): number{
		return this.roundTowDecimalNumber(subtotal * percent /100);
	}

	public roundTowDecimalNumber(price: number):number{
		return Math.round(price * 100)/100
	}
	
	public async calCart(shop_id: number, shipping_type: string, address: string, subtotal: number, 
		promo_codes: string, auto_match: number, product_items: string, open_membership: number){
		try {
			const response: any = await this.apiHelper.calCart(shop_id, shipping_type, address, subtotal, 
			promo_codes, auto_match, product_items, open_membership);
			return response;
		} catch (e) {
			console.log(e);
		}
	}
	
	public getOrderParams(): PlaceOrderParams{
	     return this.orderParams
	}

	public setOrderParams(o: PlaceOrderParams){
		this.orderParams = o;
	}
	
	public async placeOrder(){
		const response: any = await this.apiHelper.placeOrder(this.orderParams)
		return response
	}
	
	public async snapPay(orderId: number, openId: string, appId: string){
		const response: any = await this.apiHelper.snapPay(orderId, openId, appId)
		return this.chargeWXpay(response.request_payment)
	}
	
	private chargeWXpay(paymentInfo: any){
		console.log(paymentInfo)
		return new Promise((resolve, reject) => {
			uni.requestPayment({
				provider: 'wxpay',
				signType: paymentInfo.signType,
				paySign: paymentInfo.paySign,
				package: paymentInfo.package,
				nonceStr: paymentInfo.nonceStr,
				timeStamp: paymentInfo.timeStamp,
				success: res => {
					console.log(res)
					res.result = 'success'
					this.showMSG('支付成功')
					setTimeout(() => {
						resolve(res)
					}, 1500)
				},
				fail: (res) => {
					console.log(res)
					res.result = 'fail'
					this.showMSG('付款失败，请重试')
					setTimeout(() => {
						resolve(res)
					}, 1500)
				}
			})
		})
	}
	
	private showMSG(msg: string){
		uni.showToast({
			icon: 'none',
			title: msg,
			duration: 3000,
		})
	}
	
	public setPaymentInfo(data: PaymentInfo){
		this.paymentInfo = data
	}
	
	public getPaymentInfo(): PaymentInfo{
		return this.paymentInfo;
	}
	
}