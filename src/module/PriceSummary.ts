
export interface PriceSummary {
  coupon_count: number
  discount: number
  distance: number
  grand_total: number
  membership_desc: string
  org_shipping_fee: number
  org_shipping_tax: number
  org_subtotal: number
  org_subtotal_tax: number
  pure_discount: number
  service_fee: number
  service_fee_desc: string
  shipping_fee: number
  shipping_tax: number
  subtotal: number
  subtotal_tax: number
  tax: number
  product_gift: string;
  discount_desc: string;
}