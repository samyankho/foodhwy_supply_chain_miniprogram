export interface CreditCard{
	id: number;
	customer_id: number;
	card_no: string;
	expiry_year: string;
	expiry_month: string;
	zipcode: string;
	type: string;
	card_token: string;
	created_at: number;
	updated_at: number;
	can_express_payment: number;
	valid: number
}

export class CreditCard{
	public id= 0;
	public customer_id= 0;
	public card_no= "";
	public expiry_year= "";
	public expiry_month= "";
	public zipcode= "";
	public type= "";
	public card_token= "";
	public created_at= 0;
	public updated_at= 0;
	public can_express_payment= 0;
	public valid= 0
}
