import {User} from "@/module/User";
import {CityCategory} from "@/module/City";
import {Contact} from "@/module/Contact";
import {MemberList} from "@/module/MemberList";
import {Service} from "@/api/Service";

export class GlobalSetting {

  private static instance: GlobalSetting;
  // private suupply_chain_mid: number = 17243
  // 29516
  // 16755
  // 26825
  private mid: number = 17243
  
  private APPID: string = 'wxd79a7ecdac98e6a1';
  private user: User = new User();
  private contact!:Contact;
  //记录用户在地址输入框选择的地址
  private address:string = ""
  private openId:string = ""
  private cityId:number = 0
  private systemInfo:object = {}
  private userLatLng:{lat:number,lng:number} = {lat:0,lng:0}
  //用户的导航栏地址列表
  private addressCategory:CityCategory[] = []

  private constructor() {
    this.initUser();
  }
  
  public getEnvironment(){
  	  return Service.FormalServer
  	  // return Service.SandboxServer
  	  // return Service.TestServer
  }
  
  public getMid(){
	  return this.mid
  }
  
  public setMid(mid: number){
  	  this.mid = mid
  }
  
  // 设置云端环境
  public setCloudEnv(){
	wx.cloud.init({
		traceUser: true,
		env: 'foodhw-supply-chain-4c8e9a41b582',
	})
  }
  
  // 调用云函数获取openid
  public fetchUserOpenId(){
	wx.cloud.callFunction({
		name: 'getOpenId',
		success: res => {
			// @ts-ignore
			this.openId = res.result.openid
			// @ts-ignore
			this.setUserOpenIdStorage(res.result.openid)
			// @ts-ignore
			// console.log(this.getUserOpenId())
		}, 
		fail: err => {
			console.error('[云函数] [login] 调用失败', err)
		}
	})
  }
  
  //拿到用户的经纬度
  public fetchUserLocation(){
	  uni.getLocation({
	  	type: 'wgs84',
	  	altitude: true,
	  	success: res => {
			// @ts-ignore
			this.setUserLatLng(res.latitude, res.longitude)
			// @ts-ignore
			this.setUserLatLngStorage(res.latitude, res.longitude)
			// console.log(this.getUserLatLng())
	  	},
	  	fail: res=>{
	  		console.log(res);
	  		uni.showToast({
	  			icon: 'none',
	  			title: '您取消了位置授权，无法获取您附近的优惠订单。'
	  		});
	  	}
	  })
  }
  
  public fetchSystemInfo(){
	  uni.getSystemInfo({
	  	success: res => {
			// console.log(res)
			this.systemInfo = res
	  		// this.setSystemInfoStorage(res)
	  	}
	  })
  }

  //测试用户名单，无需接验证码
  public testGroup:{phone_num:string,password:string}[] = [
    {
      phone_num:'6479891678',
      password: '049054a56b'
    },
  ]

  public setContact(c: Contact){
    this.contact = c
    this.setContactStorage(c);
  }

  public setContactStorage(c: Contact){
    uni.setStorageSync('contact', JSON.stringify(c));
  }

  public getContact(): Contact{
    if(!this.contact){
      //如果没有信息，默认读取 user
      this.contact = new Contact();
      this.contact.name = this.user.name;
      this.contact.tel = this.user.phone;
      this.contact.addr = this.user.address;
    }
    return this.contact
  }

  public static getInstance(){
    if(!this.instance){
      this.instance = new GlobalSetting();
    }
    return this.instance;
  }
  
  public getAPPID(){
	  return this.APPID
  }

  public getUser() {
    return this.user
  }

  public isLogin(): boolean{
    return !!this.getUser().token
  }

  public isMember(): boolean{
	  let result = false
	  MemberList.map(id => {
	  	if (id === this.getUser().cid) {
	  		result =  true
	  	}
	  })
	  return result
  }

  public getUserLatLng(){
    return this.userLatLng
  }


  public getAddressInfo() {
    const address = this.address
    const cityId = this.cityId
    return {
      address,
      cityId
    }
  }

  public getAddressCategory() {
    return this.addressCategory
  }
  
  public getUserOpenId(){
	  return this.openId
  }
  
  public getSystemInfo(){
	  return this.systemInfo
  }

  protected initUser() {
    const sessionUser = uni.getStorageSync('user')
    const sessionAddress = uni.getStorageSync('address')
    const sessionCityId = uni.getStorageSync('city_id')
    const sessionAddressCategory = uni.getStorageSync('address_category')
    const sessionLatLng = uni.getStorageSync('user_latlng')
    const sessionContact = uni.getStorageSync('contact')
	const storageOpenId = uni.getStorageSync('openId')

    this.user = sessionUser
      ? User.initUser(JSON.parse(sessionUser)) // 有缓存User就用缓存User
      : new User();           // 无缓存就创建一个空User

    this.address = sessionAddress
      ? JSON.parse(sessionAddress)
      : ""

    this.cityId = sessionCityId
      ? JSON.parse(sessionCityId)
      : 0

    this.addressCategory = sessionAddressCategory
      ? JSON.parse(sessionAddressCategory)
      : []

    this.userLatLng = sessionLatLng
      ? JSON.parse(sessionLatLng)
      : {lat: 0, lng: 0}

    this.contact = sessionContact
      ? JSON.parse(sessionContact)
      : new Contact()
	  
	this.openId = storageOpenId
	  ? JSON.parse(storageOpenId)
	  : ""
  }



  public setUser(user: User) {
    this.user = user;}

  public clearUser() {
    this.user = new User()
    this.address = ""
    this.cityId = 0
    this.userLatLng = {lat:0,lng:0}
  }

  public setUserStorage(user: User) {
    uni.setStorageSync('user', JSON.stringify(user));
  }

  public clearUserStorage(){
    uni.removeStorageSync('user')
    uni.removeStorageSync('address')
    uni.removeStorageSync('city_id')
    uni.removeStorageSync('address_category')
    uni.removeStorageSync('user_latlng')
    uni.removeStorageSync('shopping_cart')
    uni.removeStorageSync('contact')
  }

  public setUserAddressInfo(address:string,city_id:number){
      this.address = address
      this.cityId = city_id
  }

  public setUserAddressStorage(address:string,city_id:number){
    uni.setStorageSync('address', JSON.stringify(address));
    uni.setStorageSync('city_id', JSON.stringify(city_id));
  }

  public setUserAddressListStorage(addressList:CityCategory[]){
    uni.setStorageSync('address_category', JSON.stringify(addressList));
  }

  public setUserLatLng(lat:number,lng:number){
    this.userLatLng.lat = lat
    this.userLatLng.lng = lng
  }

  public setUserLatLngStorage(lat:number,lng:number){
    uni.setStorageSync('user_latlng', JSON.stringify({lat:lat,lng:lng}));
  }
  
  public setUserOpenIdStorage(openId: string){
	uni.setStorage({	//将用户信息保存在本地
		key: 'openId',  
		data: JSON.stringify(openId)
	}) 
  }
  
  public setSystemInfoStorage(SystemInfo: any){
  	uni.setStorage({	//将用户信息保存在本地
  		key: 'SystemInfo',  
  		data: JSON.stringify(SystemInfo)
  	}) 
  }
  
  public roundTowDecimalNumber(price: number):number{
       return Math.round(price * 100)/100
  }

}






